package lights.services;

import lights.algorithms.Algorithm;

public interface LightService extends AlgorithmUpdater
{
    void setEnabled(boolean enabled);

    void start(Algorithm initialAlgorithm);
}
