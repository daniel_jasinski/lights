package lights.services;

import java.util.*;
import javax.inject.Inject;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.*;
import lights.database.*;
import lights.hardware.Lights;
import lights.model.Light;

@Component
class LightPersistanceServiceImpl implements LightPersistanceService
{
    private final LightPersister database;
    private final Lights lights;

    private final List<LightDB> lightsdb;

    @Inject public LightPersistanceServiceImpl(LightPersister database, Lights lights)
    {
        this.database = database;
        this.lights = lights;
        this.lightsdb = initializeDatabase();
    }

    private List<LightDB> initializeDatabase()
    {
        int n = lights.numberOfLights();
        List<LightDB> lights = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            lights.add(new LightDB());
        persistImpl(lights);
        return lights;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ) protected void persistImpl(List<LightDB> lights)
    {
        for (ListIterator<LightDB> iterator = lights.listIterator(); iterator.hasNext();)
        {
            LightDB db = iterator.next();
            iterator.set(database.save(db));
        }
    }

    public void persist(List<Light> lights)
    {
        assert lights.size() == lightsdb.size() : "Wrong number of lights";
        for (int i = 0; i < lights.size(); i++)
        {
            LightDB db = lightsdb.get(i);
            Light l = lights.get(i);
            db.setRed(l.red);
            db.setOrange(l.orange);
            db.setGreen(l.green);
        }
        persistImpl(lightsdb);
    }
}
