package lights.services;

import java.util.*;
import java.util.concurrent.*;
import javax.inject.Inject;
import org.springframework.stereotype.Component;
import lights.algorithms.*;
import lights.hardware.Lights;
import lights.model.Light;

@Component
class LightServiceImpl implements LightService
{
    private final static Algorithm DISABLED = new Disabled();

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private Algorithm selectedAlgorithm = DISABLED;
    private Algorithm workingAlgorithm = selectedAlgorithm;
    private State lastState;

    private final LightPersistanceService database;
    private final Lights lights;

    private boolean enabled = true;
    private boolean started = false;

    @Inject public LightServiceImpl(Lights lights, LightPersistanceService database)
    {
        this.database = database;
        this.lights = lights;
        this.lastState = State.initialFor(lights.numberOfLights());
    }

    @Override public void start(Algorithm initialAlgorithm)
    {
        if (started)
            throw new IllegalStateException("Service already started");
        started = true;
        setAlgorithm(initialAlgorithm);
        scheduleNextChange();
    }

    private void scheduleNextChange()
    {
        executor.schedule(this::updateLights, lastState.changesAfterMiliseconds(), TimeUnit.MILLISECONDS);
    }

    private void updateLights()
    {
        State state = updateState();

        List<Light> lightState = state.state();
        lights.changeLights(lightState);
        database.persist(lightState);

        scheduleNextChange();
    }

    private synchronized State updateState()
    {
        lastState = lastState.next();
        return lastState;
    }

    private synchronized void reloadState()
    {
        lastState = workingAlgorithm.initializeFor(lights.numberOfLights());
    }

    @Override public synchronized void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
        if (enabled)
            workingAlgorithm = selectedAlgorithm;
        else
            workingAlgorithm = DISABLED;
        reloadState();
    }

    @Override public synchronized void setAlgorithm(Algorithm algorithm)
    {
        this.selectedAlgorithm = algorithm;
        if (enabled)
        {
            this.workingAlgorithm = algorithm;
            reloadState();
        }
    }

}
