package lights.services;

import java.util.List;
import lights.model.Light;

public interface LightPersistanceService
{
    void persist(List<Light> lights);
}
