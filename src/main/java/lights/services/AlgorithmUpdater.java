package lights.services;

import lights.algorithms.Algorithm;

public interface AlgorithmUpdater
{
    void setAlgorithm(Algorithm algorithm);
}
