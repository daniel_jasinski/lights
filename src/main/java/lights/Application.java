package lights;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;
import lights.algorithms.Algorithm;
import lights.algorithms.AlgorithmProvider;
import lights.services.LightService;

@SpringBootApplication
public class Application implements CommandLineRunner
{
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }

    @Inject private LightService service;
    @Inject private AlgorithmProvider algorithms;

    private Algorithm selectStartingAlgorithm()
    {
        List<Algorithm> algList = algorithms.allAlgorithms();
        assert !algList.isEmpty() : "No algorithms are available";
        Random rand = new Random();
        int id = rand.nextInt(algList.size());
        return algList.get(id);
    }

    @Override public void run(String... arg0) throws Exception
    {
        service.start(selectStartingAlgorithm());
    }
}
