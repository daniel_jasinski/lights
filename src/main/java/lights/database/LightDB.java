package lights.database;

import javax.persistence.*;
import javax.persistence.Id;

@Entity(name = "light")
public class LightDB
{
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private boolean red;
    private boolean orange;
    private boolean green;

    public LightDB()
    {
    }

    public LightDB(boolean red, boolean orange, boolean green)
    {
        this.red = red;
        this.orange = orange;
        this.green = green;
    }

    public boolean isRed()
    {
        return red;
    }

    public void setRed(boolean red)
    {
        this.red = red;
    }

    public boolean isOrange()
    {
        return orange;
    }

    public void setOrange(boolean orange)
    {
        this.orange = orange;
    }

    public boolean isGreen()
    {
        return green;
    }

    public void setGreen(boolean green)
    {
        this.green = green;
    }
}
