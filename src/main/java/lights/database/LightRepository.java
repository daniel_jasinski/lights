package lights.database;

import java.util.List;
import org.springframework.data.repository.Repository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "lights")
public interface LightRepository extends Repository<LightDB, Integer>
{
    List<LightDB> findAll();

    @RestResource(path = "red", rel = "red")
    List<LightDB> findByRedTrue();

    @RestResource(path = "orange", rel = "orange")
    List<LightDB> findByOrangeTrue();

    @RestResource(path = "green", rel = "green")
    List<LightDB> findByGreenTrue();
}
