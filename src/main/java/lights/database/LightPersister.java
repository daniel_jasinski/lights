package lights.database;

import org.springframework.data.repository.*;

public interface LightPersister extends CrudRepository<LightDB, Integer>
{
}
