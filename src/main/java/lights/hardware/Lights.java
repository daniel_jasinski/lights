package lights.hardware;

import java.util.List;

import lights.model.Light;

public interface Lights
{
    int numberOfLights();

    void changeLight(int id, Light light);

    default void changeLights(List<Light> lights)
    {
        for (int i = 0; i < lights.size(); i++)
            changeLight(i, lights.get(i));
    }
}
