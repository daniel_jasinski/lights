package lights.hardware;

import javax.inject.Inject;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.*;
import lights.model.Light;

@Component
@Configuration
@PropertySource(DummyLights.CONFIG_FILE_LOCATION)
class DummyLights implements Lights
{
	final static String NUMBER_OF_LIGHTS_KEY = "crossroad.entrances";
	final static String CONFIG_FILE_LOCATION = "classpath:crossroad.properties";
	
	private final int n;
	
	@Inject public DummyLights(Environment env)
	{
		n = env.getProperty(NUMBER_OF_LIGHTS_KEY, int.class);
	}

	public int numberOfLights()
	{
		return n;
	}
	
	public void changeLight(int id, Light light)
	{
		System.out.println("Changed light: "+id+" to: "+light);
	}
}
