package lights.algorithms;

@SuppressWarnings("serial")
public class InvalidAlgorithmName extends Exception
{
    public InvalidAlgorithmName()
    {
    }

    public InvalidAlgorithmName(String message)
    {
        super(message);
    }
}
