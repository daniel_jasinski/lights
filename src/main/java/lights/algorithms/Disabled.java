package lights.algorithms;

import java.util.*;
import lights.model.Light;
import static lights.model.Light.*;

public class Disabled implements Algorithm
{
    private final static int BLINK_INTERVAL = 1000;

    @Override public State initializeFor(int n)
    {
        // we are starting with orange on
        return new DisabledState(n, true);
    }

    private static class DisabledState implements State
    {
        private final int n;
        private final boolean on;

        public DisabledState(int n, boolean on)
        {
            this.n = n;
            this.on = on;
        }

        @Override public State next()
        {
            return new DisabledState(n, !on);
        }

        @Override public List<Light> state()
        {
            return Collections.nCopies(n, on ? ORANGE : OFF);
        }

        @Override public int changesAfterMiliseconds()
        {
            return BLINK_INTERVAL;
        }
    }
}
