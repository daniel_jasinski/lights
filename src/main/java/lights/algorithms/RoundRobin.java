package lights.algorithms;

import org.springframework.stereotype.Component;
import java.util.*;
import lights.model.Light;
import static lights.model.Light.*;

@Component("robin")
public class RoundRobin implements Algorithm
{
    @Override public State initializeFor(int n)
    {
        return new Transition(n, 0);
    }

    private static List<Light> nCopies(int n, Light element)
    {
        List<Light> out = new ArrayList<>();
        for (int i = 0; i < n; i++)
            out.add(element);
        return out;
    }

    private static class Normal implements State
    {
        private final int n;
        private final int current;

        private Normal(int n, int current)
        {
            this.n = n;
            this.current = current;
        }

        @Override public State next()
        {
            return new Transition(n, current);
        }

        @Override public List<Light> state()
        {
            List<Light> out = nCopies(n, RED);
            out.set(current, GREEN);
            return out;
        }

        @Override public int changesAfterMiliseconds()
        {
            return NORMAL_DURATION;
        }
    }

    private static class Transition implements State
    {
        private final int n;
        private final int current;

        private Transition(int n, int current)
        {
            this.n = n;
            this.current = current;
        }

        private int nextId()
        {
            return (current + 1) % n;
        }

        @Override public State next()
        {
            return new Normal(n, nextId());
        }

        @Override public List<Light> state()
        {
            List<Light> out = nCopies(n, RED);
            out.set(current, ORANGE);
            out.set(nextId(), GREEN_NEXT);
            return out;
        }

        @Override public int changesAfterMiliseconds()
        {
            return TRANSITION_DURATION;
        }
    }
}
