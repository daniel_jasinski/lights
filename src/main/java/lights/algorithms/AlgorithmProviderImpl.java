package lights.algorithms;

import org.springframework.stereotype.Component;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;
import lights.hardware.Lights;

@Component
class AlgorithmProviderImpl implements AlgorithmProvider
{
    @Inject private Map<String, Algorithm> algorithms;
    @Inject private Lights lights;

    @Override public List<Algorithm> allAlgorithms()
    {
        return algorithms.values().stream()
            .filter(a -> a.supportsNumberOfLights(lights.numberOfLights()))
            .collect(Collectors.toList());
    }

    public Algorithm byName(String name) throws InvalidAlgorithmName
    {
        Algorithm a = algorithms.get(name);
        if (a == null)
            throw new InvalidAlgorithmName("No such algorithm: " + name);
        if (!a.supportsNumberOfLights(lights.numberOfLights()))
            throw new InvalidAlgorithmName("Algorithm '" + name + "' is not supported on this crossroad.");
        return a;
    }
}
