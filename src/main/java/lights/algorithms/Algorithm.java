package lights.algorithms;

public interface Algorithm
{
    int NORMAL_DURATION = 20_000;
    int TRANSITION_DURATION = 4_000;

    State initializeFor(int n);

    default boolean supportsNumberOfLights(int n)
    {
        return true;
    }
}
