package lights.algorithms;

import java.util.List;

public interface AlgorithmProvider
{
    List<Algorithm> allAlgorithms();

    Algorithm byName(String name) throws InvalidAlgorithmName;
}
