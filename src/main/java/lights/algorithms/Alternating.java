package lights.algorithms;

import java.util.*;
import org.springframework.stereotype.Component;
import lights.model.Light;
import static lights.model.Light.*;

@Component("alternating")
public class Alternating implements Algorithm
{
    @Override public boolean supportsNumberOfLights(int n)
    {
        return isEven(n);
    }

    @Override public State initializeFor(int n)
    {
        return new Transition(n, true);
    }

    private static boolean isEven(int n)
    {
        return n % 2 == 0;
    }

    private static class Normal implements State
    {
        private final int n;
        private final boolean even;

        private Normal(int n, boolean even)
        {
            this.n = n;
            this.even = even;
        }

        @Override public State next()
        {
            return new Transition(n, even);
        }

        @Override public List<Light> state()
        {
            List<Light> out = new ArrayList<>(n);
            for (int i = 0; i < n; i++)
            {
                boolean on = even == isEven(i);
                out.add(on ? GREEN : RED);
            }
            return out;
        }

        @Override public int changesAfterMiliseconds()
        {
            return NORMAL_DURATION;
        }
    }

    private static class Transition implements State
    {
        private final int n;
        private final boolean even;

        private Transition(int n, boolean even)
        {
            this.n = n;
            this.even = even;
        }

        @Override public State next()
        {
            return new Normal(n, !even);
        }

        @Override public List<Light> state()
        {
            List<Light> out = new ArrayList<>(n);
            for (int i = 0; i < n; i++)
            {
                boolean on = even == isEven(i);
                out.add(on ? ORANGE : GREEN_NEXT);
            }
            return out;
        }

        @Override public int changesAfterMiliseconds()
        {
            return TRANSITION_DURATION;
        }
    }
}
