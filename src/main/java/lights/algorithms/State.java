package lights.algorithms;

import java.util.*;
import lights.model.Light;

public interface State
{
    State next();

    List<Light> state();

    int changesAfterMiliseconds();

    static State initialFor(int n)
    {
        return new InitialState(n);
    }
}

class InitialState implements State
{
    private final int n;

    InitialState(int n)
    {
        this.n = n;
    }

    @Override public State next()
    {
        return this;
    }

    @Override public List<Light> state()
    {
        return Collections.nCopies(n, Light.OFF);
    }

    @Override public int changesAfterMiliseconds()
    {
        return Integer.MAX_VALUE;
    }
}
