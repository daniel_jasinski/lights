package lights.model;

public class Light
{
    public final static Light OFF = new Light(false, false, false);
    public final static Light RED = new Light(true, false, false);
    public final static Light ORANGE = new Light(false, true, false);
    public final static Light GREEN = new Light(false, false, true);
    public final static Light GREEN_NEXT = new Light(true, true, false);

    public final boolean red;
    public final boolean orange;
    public final boolean green;

    private Light(boolean red, boolean orange, boolean green)
    {
        this.red = red;
        this.orange = orange;
        this.green = green;
    }

    @Override public String toString()
    {
        return "[red=" + red + ", orange=" + orange + ", green=" + green + "]";
    }
}
