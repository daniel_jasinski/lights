package lights.rest;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import javax.inject.*;
import lights.algorithms.*;
import lights.services.AlgorithmUpdater;

@RestController
public class AlgorithmController
{
    @Inject private AlgorithmProvider algorithms;
    @Inject private AlgorithmUpdater service;

    @RequestMapping(path = "/method/{name}", method = RequestMethod.POST)
    public ResponseEntity<?> selectAlgorithm(@PathVariable("name") String name)
    {
        try
        {
            service.setAlgorithm(algorithms.byName(name));
            return new ResponseEntity<>("Algorithm switched to: " + name, HttpStatus.OK);
        }
        catch (InvalidAlgorithmName e)
        {
            return new ResponseEntity<>("Invalid algorithm: " + name, HttpStatus.BAD_REQUEST);
        }
    }
}
