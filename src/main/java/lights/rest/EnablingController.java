package lights.rest;

import org.springframework.web.bind.annotation.*;
import javax.inject.*;
import lights.services.LightService;

@RestController
public class EnablingController
{
    @Inject private LightService service;

    @RequestMapping(path = "/switch", method = RequestMethod.POST)
    @ResponseBody
    public void enableLights(@RequestParam(value = "on") boolean enabled)
    {
        service.setEnabled(enabled);
    }
}
