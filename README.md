# README #

Prosta aplikacja do zarządzania światłami na rondzie.

### Wymagania ###

* aplikacja w trakcie działania sama zmienia światła
* dwa algorytmy sterujące światłami, dostępne algorytmy zależą od ilości wjazdów na rondo
* przy każdej zmianie świateł ich stan zapisywany w bazie
* w każdym momencie usługą REST możemy pobrać stan wszystkich świateł lub pobrać światła, które w danym momencie mają określony kolor (dane pobierane z bazy)
* usługa REST wyłączającą i włączającą światła (pomarańczowe migające gdy wyłączone)
* usługa REST pobierający i zmieniającą algorytm sterujący światłami
* liczba wjazdów na rondo jest konfigurowalna przez plik z properties-ami
 
### Technologie ###

* Spring Boot
* Spring Data
* Embedded Database